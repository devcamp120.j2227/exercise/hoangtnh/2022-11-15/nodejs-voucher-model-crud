//Import thư viện mongoose
const mongoose = require ("mongoose");

//class Schema từ thư viện mongoose
const Schema = mongoose.Schema;

//khởi tạo userSchema từ class Schema
const userSchema = new Schema ({
    fullName: {
        type: String, 
        required: true
    },
	email: {
        type: String, 
        required: true, 
        unique: true
    },
    address: {
        type: String, 
        required: true
    },
    phone: {
        type: String, 
        required: true, 
        unique: true
    },
    orders: {
        type: mongoose.Types.ObjectId,
        ref: "Order",
    }
});
//Biên dịch user model từ userSchema
module.exports = mongoose.model("User", userSchema);