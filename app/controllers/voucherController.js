//Import thư viện Mongoose
const mongoose = require("mongoose");
//Import Voucher model
const voucherModel = require("../model/voucherModel");

//function createVoucher
const createVoucher = (request,response) =>{
    //B1: chuẩn bị dữ liệu
    const body = request.body;
    console.log(body);
    //B2: validate dữ liệu
    if(!body.maVoucher){
        return response.status(400).json({
            status: "Bad request",
            message: "Mã voucher không hợp lệ"
        })
    }
    if(isNaN(body.phanTramGiamGia)||body.phanTramGiamGia < 0){
        return response.status(400).json({
            status: "Bad request",
            message: "Phần trăm giảm giá không hợp lệ"
        })
    }

    //B3: gọi model tạo dữ liệu mới
    const newVoucher = {
        _id: mongoose.Types.ObjectId(),
        maVoucher: body.maVoucher,
        phanTramGiamGia: body.phanTramGiamGia,
        ghiChu: body.ghiChu
    }
    voucherModel.create(newVoucher, (error, data) =>{
        if(error){
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        return response.status(201).json({
            status: "Create new voucher successfully",
            data: data
        })
    })
}

//function get all voucher
const getAllVoucher = (request,response) =>{
    voucherModel.find((error, data)=>{
        if(error){
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        return response.status(200).json({
            status: "Get all voucher successfully",
            data: data
        })
    })
}

//function get voucher by id
const getVoucherById = (request, response) => {
    //B1: chuẩn bị dữ liệu
    const voucherId = request.params.voucherId;
    //B2: validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(voucherId)){
        return response.status(400).json({
            status: "Bad Request",
            message: "voucherId không hợp lệ"
        })
    }
    //B3: gọi model tạo dữ liệu
    voucherModel.findById(voucherId,(error, data)=>{
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        return response.status(200).json({
            status: "Get detail voucher successfully",
            data: data
        })
    })
}

//function update voucher by id
const updateVoucherById = (request, response) =>{
    //B1: chuẩn bị dữ liệu
    const voucherId = request.params.voucherId;
    const body = request.body;

    //B2: validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(voucherId)){
        return response.status(400).json({
            status: "Bad Request",
            message: "voucherId không hợp lệ"
        })
    }
    if(!body.maVoucher){
        return response.status(400).json({
            status: "Bad request",
            message: "Mã voucher không hợp lệ"
        })
    }
    if(isNaN(body.phanTramGiamGia)||body.phanTramGiamGia < 0){
        return response.status(400).json({
            status: "Bad request",
            message: "Phần trăm giảm giá không hợp lệ"
        })
    }

    //B3: gọi model tạo dữ liệu
    const updateVoucher = {}
    if(body.maVoucher !== undefined){
        updateVoucher.maVoucher = body.maVoucher
    }
    if(body.phanTramGiamGia !== undefined){
        updateVoucher.phanTramGiamGia = body.phanTramGiamGia
    }
    if(body.ghiChu !== undefined){
        updateVoucher.ghiChu = body.ghiChu
    }

    voucherModel.findByIdAndUpdate(voucherId,updateVoucher,(error,data)=>{
        if(error){
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }
        return response.status(200).json({
            status: "Update voucher successfully",
            data: data
        })
    })
}

//function delete voucher by id
const deleteVoucherById = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const voucherId = request.params.voucherId;

    // B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(voucherId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "voucherId không hợp lệ"
        })
    }

    // B3: Gọi Model tạo dữ liệu
    voucherModel.findByIdAndDelete(voucherId, (error, data) => {
        if(error) {
            return response.status(500).json({
                status: "Internal server error",
                message: error.message
            })
        }

        return response.status(200).json({
            status: "Delete voucher successfully"
        })
    })
}

module.exports={
    createVoucher,
    getAllVoucher,
    getVoucherById,
    updateVoucherById,
    deleteVoucherById
}